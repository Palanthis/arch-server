#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

# Install SAMBA and OpenSSH
sudo pacman -S --needed --noconfirm samba openssh

# Copy default config for Samba
sudo cp smb.conf /etc/samba/smb.conf

# Create SMB User
echo
echo "What SMB user should we create?"
read input
sudo smbpasswd -a $input

# Enable Services
sudo systemctl enable smb.service
sudo systemctl enable nmb.service
sudo systemctl enable sshd.service

# Inform of nest steps
echo
echo "You now need to create your folder(s) to be shared."
echo "Then you will define them at the end of /etc/samba/smb.conf."
echo "Finally restart samba with the following commands."
echo "sudo systemctl restart smb.service"
echo "sudo systemctl restart nmb.service"
